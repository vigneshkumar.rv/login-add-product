import React, { Component } from 'react';
import { Text, ScrollView, AsyncStorage, TouchableOpacity, ToastAndroid, TextInput } from "react-native"
import { Container, Icon, Col, Body, Header, Button, Content, Right, View, Fab, Left, Title, Tabs, ScrollableTab, InputGroup, Input, Footer } from 'native-base';
import appStyle from "../../styles/appStyle";
// import PageLoader from "../../components/Loader";
import Modal from 'react-native-modal';


class HomePage extends Component {
    constructor() {
        super();
        this.state = {
            productList: [],
            name: "",
            rate: "",
            quality: "",
            modal: false,
            loading: false,
            userName: ""
        }
    }


    componentDidMount = async () => {
        const { userName } = this.props.route.params;
        let getName = userName.replace(/^(.+)@(.+)$/g, '$1');
        this.setState({ userName: getName });
        this.initialLoadData();
    }

    initialLoadData = async () => {
        const getProductList = await AsyncStorage.getItem('productList');
        if (getProductList) {
            const productList = JSON.parse(getProductList);
            this.setState({ productList, loading: false });
        }
    }

    deleteProduct(val) {
        let recard = [];
        this.state.productList.map((data, index) => {
            if (data.id !== val.id) {
                this.setState({ productList: data })
                ToastAndroid.show("Deleted successfully", ToastAndroid.SHORT);
                recard.push(data)
            }
        });
        AsyncStorage.setItem('productList', JSON.stringify(recard));
        this.setState({ productList: recard })
    }

    toggleModal = () => {
        this.setState({ modal: !this.state.modal });
    };


    addProduct = async () => {
        let keyData = (((1 + Math.random()) * 0x10000) | 0);
        if (this.state.name === '') {
            ToastAndroid.show('Enter a name', ToastAndroid.SHORT);
        } else if (this.state.rate === '') {
            ToastAndroid.show('Enter a rate', ToastAndroid.SHORT);
        } else if (this.state.quality === '') {
            ToastAndroid.show('Enter a quality', ToastAndroid.SHORT);
        }
        if (this.state.name && this.state.rate && this.state.quality) {
            this.state.productList.push({ id: keyData.toString(), name: this.state.name, rate: this.state.rate, quality: this.state.quality });
            AsyncStorage.setItem('productList', JSON.stringify(this.state.productList));
            ToastAndroid.show('Saved successfully', ToastAndroid.SHORT);
            this.setState({ modal: false });
        }
    }

    render() {
        return (
            <Container >
                <Header noShadow>
                    <Left>
                        <Button transparent>
                            <Icon name='menu' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Test App</Title>
                    </Body>
                    <Right><Text style={appStyle.textWhite}>Hello: {this.state.userName}</Text></Right>
                </Header>
                <Content padder style={appStyle.bgGray}>
                    <ScrollView>
                        {this.state.productList !== undefined &&
                            this.state.productList !== null &&
                            this.state.productList.length > 0 &&
                            this.state.productList.map((data, index) => (
                                <TouchableOpacity key={index} style={[appStyle.card]}>
                                    <Col>
                                        <Text style={[appStyle.font12]}>Name</Text>
                                        <Text style={[appStyle.font16, appStyle.pB5]}>{data.name}</Text>
                                    </Col>
                                    <Col>
                                        <Text style={[appStyle.font12]}>Quality</Text>
                                        <Text style={[appStyle.font16, appStyle.pB5]}>{data.quality}</Text>
                                    </Col>
                                    <Col>
                                        <Text style={[appStyle.font12]}>Rate</Text>
                                        <Text style={[appStyle.font16, appStyle.pB5]}>{data.rate}</Text>
                                    </Col>
                                    <Col style={[appStyle.fDRow, { width: 50, marginLeft: 'auto' }]}>
                                        <TouchableOpacity style={[appStyle.deleteBtn]} onPress={() => this.deleteProduct(data, data.id)}>
                                            <Icon name='trash' style={[appStyle.font20, { color: 'red' }]} />
                                        </TouchableOpacity>
                                    </Col>
                                </TouchableOpacity>
                            ))}
                        {
                            this.state.productList.length === 0 && (
                                <View padder style={{ height: 250, alignItems: 'center' }}>
                                    <Text style={[appStyle.font14, { top: 100 }]}>No Records Found!</Text>
                                </View>
                            )
                        }
                    </ScrollView>
                    <View style={appStyle.fDRow}>
                        <Modal isVisible={this.state.modal}>
                            <View style={[appStyle.bgWhite, { flex: 1 }]}>
                                <Content>
                                    <View style={[appStyle.alignItemsCenter]}>
                                        <Text style={[appStyle.font20b, appStyle.pTB20]}>Add Product</Text>
                                    </View>
                                    <View style={[appStyle.pL15, appStyle.pR20, appStyle.mT20]}>
                                        <Text>Name:</Text>
                                        <TextInput
                                            style={appStyle.inputs}
                                            onChangeText={(text) => this.setState({ name: text })}
                                        />
                                    </View>
                                    <View style={[appStyle.pL15, appStyle.pR20, appStyle.pT20]}>
                                        <Text>Rate:</Text>
                                        <TextInput
                                            style={appStyle.inputs}
                                            onChangeText={(text) => this.setState({ rate: text })}
                                        />
                                    </View>
                                    <View style={[appStyle.pL15, appStyle.pR20, appStyle.pT20]}>
                                        <Text>Quality:</Text>
                                        <TextInput
                                            style={appStyle.inputs}
                                            onChangeText={(text) => this.setState({ quality: text })}
                                        />
                                    </View>
                                </Content>
                                <Footer style={[appStyle.fDRow, { backgroundColor: "#fff", height: 45 }]}>
                                    <Col>
                                        <Button onPress={this.toggleModal} style={[appStyle.jCCenter, appStyle.bgGray]}>
                                            <Text style={[appStyle.textBlack, appStyle.font18]}>Close</Text>
                                        </Button>
                                    </Col>
                                    <Col>
                                        <Button onPress={() => this.addProduct()} style={[appStyle.jCCenter]}>
                                            <Text style={[appStyle.textWhite, appStyle.font18]}>Save</Text>
                                        </Button>
                                    </Col>
                                </Footer>
                            </View>
                        </Modal>
                    </View>
                </Content>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{}}
                    style={{ backgroundColor: '#5067FF' }}
                    position="bottomRight"
                    onPress={this.toggleModal}>
                    <Icon name="add" />
                </Fab>
            </Container>
        );
    }
}
export default HomePage; 