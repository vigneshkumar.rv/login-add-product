import React, { Component } from 'react';
import { Keyboard, ToastAndroid, Text, View, TextInput, TouchableWithoutFeedback, Alert, KeyboardAvoidingView } from 'react-native';
import { Button, Container } from 'native-base';
import appStyle from '../../styles/appStyle';

class LoginPage extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
        }
    }

    onLoginPress() {
        if (this.state.email === '') {
            ToastAndroid.show('Enter a email', ToastAndroid.SHORT);
        } else if (this.state.password === '') {
            ToastAndroid.show('Enter a password', ToastAndroid.SHORT);
        }
        if (this.state.email === "Clarion@clarion.com" || this.state.email === "clarion@clarion.com" && this.state.password === "Clarion123") {
            this.props.navigation.navigate('HomePage', { 'userName': this.state.email });
            ToastAndroid.show('Login successful', ToastAndroid.SHORT);
        } else {
            ToastAndroid.show('Email or password is incorrect', ToastAndroid.SHORT);
        }
    }



    render() {

        return (
            <Container>
                <View style={appStyle.containerView}>
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                        <View style={appStyle.loginFormView}>
                            <Text style={appStyle.logoText}>Test App</Text>
                            <TextInput keyboardType="email-address" placeholder="Username" placeholderColor="#c4c3cb"
                                onChangeText={(text) => this.setState({ email: text })}
                                style={appStyle.loginFormTextInput} />
                            <TextInput placeholder="Password" placeholderColor="#c4c3cb"
                                onChangeText={(text) => this.setState({ password: text })}
                                style={appStyle.loginFormTextInput} secureTextEntry={true} />
                            <Button
                                style={appStyle.loginButton}
                                onPress={() => this.onLoginPress()}
                            >
                                <Text style={[appStyle.alignSelfCenter, appStyle.textWhite]}>Login</Text>
                            </Button>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </Container>
        );
    }
}
export default LoginPage;

