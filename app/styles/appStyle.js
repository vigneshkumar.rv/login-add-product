const React = require("react-native");

const { Platform } = React;
const platform = Platform.OS;

export default {
    // ************ Header ************ 

    textUnderLine: { textDecorationLine: "underline" },
    textCenter: { textAlign: "center" },
    textRight: { textAlign: "right" },

    alignSelfCenter: { alignSelf: "center" },
    alignSelfBaseline: { alignSelf: "baseline" },
    alignSelfEnd: { alignSelf: "flex-end" },
    alignSelfStart: { alignSelf: "flex-start" },
    alignItemsFlexEnd: { alignItems: 'flex-end' },
    alignItemsCenter: { alignItems: "center" },

    italic: { fontStyle: "italic" },
    fDRow: { flexDirection: "row" },
    fDColumn: { flexDirection: "column" },

    jCCenter: { justifyContent: "center" },
    jCEnd: { justifyContent: "flex-end" },
    jCStart: { justifyContent: "flex-start" },

    // ************ Test Color ************
    textWhite: { color: "#ffffff" },
    textBlack: { color: "#000000" },
    textGold: { color: "#C89226" },
    textYellow: { color: "#F1B43B" },
    textRed: { color: "red" },
    textBlue: { color: "#2099D9" },
    textGreen: { color: "#29A433" },

    bgWhite: { backgroundColor: "#fff" },
    bgGray: { backgroundColor: "#eee" },


    // position
    pRelative: { position: 'relative' },
    pAbsolute: { position: 'absolute' },


    // ************ Font Family ************
    fontWeightNormal: { fontWeight: "300" },
    font300: { fontFamily: platform === "ios" ? "System" : "OpenSans-Light-300" },
    font400: {
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font600: {
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font700: {
        fontFamily: platform === "ios" ? "System" : "OpenSans-ExtraBold-700"
    },

    //  ************ Font Size ************ 
    font9: {
        fontSize: 9,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font10: {
        fontSize: 10,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font11: {
        fontSize: 11,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font11b: {
        fontSize: 11,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font12: {
        fontSize: 12,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font12b: {
        fontSize: 12,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font13: {
        fontSize: 13,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font13b: {
        fontSize: 13,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font14: {
        fontSize: 14,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font14b: {
        fontSize: 14,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font14eb: {
        fontSize: 14,
        fontFamily: platform === "ios" ? "System" : "OpenSans-ExtraBold-700"
    },
    font15: {
        fontSize: 15,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font15b: {
        fontSize: 15,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font16: {
        fontSize: 16,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font16b: {
        fontSize: 16,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font18b: {
        fontSize: 18,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font19b: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font20: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font20b: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font22: {
        fontSize: 22,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font22b: {
        fontSize: 22,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font25: {
        fontSize: 25,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font28: {
        fontSize: 28,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font28b: {
        fontSize: 28,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font25b: {
        fontSize: 25,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    font35: {
        fontSize: 35,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },
    font18: {
        fontSize: 18,
        fontFamily: platform === "ios" ? "System" : "OpenSans-Regular-400"
    },

    fontH1: {
        fontSize: 22,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    fontH2: {
        fontSize: 20,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    fontH3: {
        fontSize: 18,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    fontH4: {
        fontSize: 17,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },
    fontH5: {
        fontSize: 16,
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },

    fontBold: {
        fontFamily: platform === "ios" ? "System" : "OpenSans-SemiBold-600"
    },


    // ************  Padding & Margin ************ 
    mLAuto: { marginLeft: "auto" },
    mRAuto: { marginRight: "auto" },

    p0: { padding: 0 },
    pB0: { paddingBottom: 0 },
    pT0: { paddingTop: 0 },
    mL0: { marginLeft: 0 },
    pLR0: { paddingLeft: 5, paddingRight: 5 },

    pR5: { paddingRight: 5 },
    pT5: { paddingTop: 5 },
    pL5: { paddingLeft: 5 },
    pB5: { paddingBottom: 5 },
    mR5: { marginRight: 5 },
    mL5: { marginLeft: 5 },
    mT5: { marginTop: 5 },
    mT7: { marginTop: 7 },
    mB5: { marginBottom: 5 },
    pTB5: { paddingTop: 5, paddingBottom: 5 },
    p5: { paddingLeft: 5, paddingRight: 5, paddingTop: 5, paddingBottom: 5 },

    pT10: { paddingTop: 10 },
    pL10: { paddingLeft: 10 },
    pB10: { paddingBottom: 10 },
    pR10: { paddingRight: 10 },
    mR10: { marginRight: 10 },
    mT10: { marginTop: 10 },
    mB10: { marginBottom: 10 },
    mL10: { marginLeft: 10 },
    pTB10: { paddingTop: 10, paddingBottom: 10 },
    p10: { paddingLeft: 10, paddingRight: 10, paddingTop: 10, paddingBottom: 10 },
    mTB10: { marginTop: 10, marginBottom: 10 },
    pLR10: { paddingLeft: 10, paddingRight: 10 },

    pT15: { paddingTop: 15 },
    pB15: { paddingBottom: 15 },
    mT15: { marginTop: 15 },
    pR15: { paddingRight: 15 },
    pL15: { paddingLeft: 15 },
    mR15: { marginRight: 15 },
    mL15: { marginLeft: 15 },
    pLR15: { paddingLeft: 15, paddingRight: 15 },
    p15: { paddingLeft: 15, paddingRight: 15, paddingTop: 15, paddingBottom: 15 },
    p20: { paddingLeft: 20, paddingRight: 20, paddingTop: 20, paddingBottom: 20 },
    p30: { paddingLeft: 30, paddingRight: 30, paddingTop: 30, paddingBottom: 30 },
    p50: { paddingLeft: 50, paddingRight: 50, paddingTop: 50, paddingBottom: 50 },
    p60: { paddingLeft: 60, paddingRight: 60, paddingTop: 60, paddingBottom: 60 },
    pTB15: { paddingTop: 15, paddingBottom: 15 },

    pTB20: { paddingTop: 20, paddingBottom: 20 },
    pT20: { paddingTop: 20 },
    mT20: { marginTop: 20 },
    mB20: { marginBottom: 20 },
    pL20: { paddingLeft: 20 },
    mL20: { marginLeft: 20 },
    pB20: { paddingBottom: 20 },
    pR20: { paddingRight: 20 },
    mR20: { marginRight: 20 },
    mTB20: { marginTop: 20, marginBottom: 20 },
    pLR20: { paddingLeft: 20, paddingRight: 20 },
    pTBLR20: { paddingTop: 20, paddingBottom: 20, paddingLeft: 20, paddingRight: 20 },
    pTLR20: { paddingTop: 20, paddingLeft: 20, paddingRight: 20 },

    pTB25: { paddingTop: 250, paddingBottom: 25 },
    pT25: { paddingTop: 25 },
    pB25: { paddingBottom: 25 },
    mT25: { marginTop: 25 },
    mB25: { marginBottom: 25 },

    mT30: { marginTop: 30 },
    pT30: { paddingTop: 30 },
    pB30: { paddingBottom: 30 },
    mB30: { marginBottom: 30 },
    pTB30: { paddingTop: 30, paddingBottom: 30 },

    mT40: { marginTop: 40 },
    mB40: { marginBottom: 40 },

    mT50: { marginTop: 50 },
    mT60: { marginTop: 60 },
    mT80: { marginTop: 80 },

    mB6: { marginBottom: 6 },
    mB7: { marginBottom: 7 },

    // ********* Align ************
    top1: { top: 1 },
    top2: { top: 2 },
    top3: { top: 3 },
    top4: { top: 4 },
    top5: { top: 5 },
    left1: { left: 1 },
    left2: { left: 2 },
    left3: { left: 3 },
    left4: { left: 4 },
    left5: { left: 5 },
    right1: { right: 1 },
    right2: { right: 2 },
    right3: { right: 3 },
    right4: { right: 4 },
    right5: { right: 5 },
    right6: { right: 6 },
    right10: { right: 10 },
    bottom1: { bottom: 1 },
    bottom2: { bottom: 2 },
    bottom3: { bottom: 3 },
    bottom4: { bottom: 4 },
    bottom5: { bottom: 5 },

    // ************ Width and Height ************ 
    w100: { width: "100%" },
    w50: { width: "50%" },
    w90: { width: "90%" },
    w80: { width: "80%" },
    w50: { width: 50 },
    wH35: { width: 35, height: 35 },
    h20: { height: 20 },
    h22: { height: 22 },
    h24: { height: 24 },
    h25: { height: 25 },
    h30: { height: 30 },
    h40: { height: 40 },
    h50: { height: 50 },
    h55: { height: 55 },
    h100: { height: 100 },




    // Loader
    modalBackground: { flex: 1, alignItems: "center", flexDirection: "column", justifyContent: "space-around", backgroundColor: "#00000010" },
    activityIndicatorWrapper: { backgroundColor: "#fff", position: "absolute", top: 100, height: 40, width: 40, borderRadius: 40, display: "flex", alignItems: "center", justifyContent: "space-around" },


    // Input Field

    inputs: {
        height: 45,
        padding: 10,
        backgroundColor: '#fcfcfe',
        borderWidth: 1,
        borderColor: '#ddd',
        marginTop: 5
    },

    card: {
        padding: 10, backgroundColor: '#fff', elevation: 5, marginBottom: 10, borderRadius: 5, flexDirection: 'row'
    },
    deleteBtn: {
        width: 35, height: 40, alignItems: 'center', paddingTop: 10
    },

    containerView: {
        flex: 1,
    },
    logoText: {
        fontSize: 40,
        fontWeight: "800",
        marginTop: 150,
        marginBottom: 30,
        textAlign: 'center',
    },
    loginFormView: {
        flex: 1,
        paddingLeft: 30,
        paddingRight: 30,
    },
    loginFormTextInput: {
        height: 43,
        fontSize: 14,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#eaeaea',
        backgroundColor: '#fafafa',
        paddingLeft: 10,
        marginTop: 5,
        marginBottom: 5,

    },
    loginButton: {
        backgroundColor: '#3897f1',
        borderRadius: 5,
        height: 45,
        marginTop: 10,
        width: "100%",
        justifyContent: 'center'

    },
};
