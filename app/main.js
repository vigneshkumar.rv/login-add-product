import React, { Component } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



// Root Pages 
import HomePage from "./containers/HomePage";
import LoginPage from "./containers/LoginPage";

// Function for Drawer & Nav
const Stack = createStackNavigator();

function MainNavigator() {
    return (
        <Stack.Navigator initialRouteName="LoginPage"
            headerMode="none"
        >
            <Stack.Screen name="HomePage" component={HomePage} />
            <Stack.Screen name="LoginPage" component={LoginPage} />
        </Stack.Navigator>
    );
}



function MainApp() {
    return (
        <NavigationContainer>
            <MainNavigator />
        </NavigationContainer >
    );
}

export default MainApp;