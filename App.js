import React, { useEffect } from 'react';
import MainApp from "./app/main";

export default class App extends React.Component {
  render() {
    return (
      <MainApp />
    );
  }
}; 
